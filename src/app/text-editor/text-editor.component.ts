import { Component, OnInit, HostListener} from '@angular/core';
import { IStyledString, ITextStyling } from '../app.interfaces';
import { TextService } from '../text-service/text.service';

@Component({
  selector: 'app-text-editor',
  templateUrl: './text-editor.component.html',
  styleUrls: ['./text-editor.component.css']
})
export class TextEditorComponent implements OnInit {
  styledWords: IStyledString[];
  selectedWordIndex: number = null;

  constructor(private textService: TextService) { }

  get selectedStyling (): ITextStyling {
    return this.selectedWordIndex !== null ?
      this.styledWords[this.selectedWordIndex].styling :
      this.textService.defauldStyling;
  }

  get selectedWord(): IStyledString | null {
    return this.selectedWordIndex !== null ?
      this.styledWords[this.selectedWordIndex] :
      null;
  }

  ngOnInit() {
    this.styledWords = this.textService.getStylesWords();
  }

  onWordSelected(id: number): void {
    this.selectedWordIndex = id;
  }

  updateStyling(updatedStyling: ITextStyling): void {
    if (this.selectedWordIndex !== null) {
      this.styledWords[this.selectedWordIndex] = {
        ...this.styledWords[this.selectedWordIndex],
        styling: updatedStyling
      };
    }
  }

  updateSelectedWord(updatedWord: string): void {
    if (this.selectedWordIndex !== null) {
      this.styledWords[this.selectedWordIndex] = {
        ...this.styledWords[this.selectedWordIndex],
        content: updatedWord
      };
    }
  }

  @HostListener('document:click')
  resetSelectedWord(): void {
    this.selectedWordIndex = null;
  }
}
