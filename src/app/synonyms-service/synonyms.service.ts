import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class SynonymsService {
  private baseUrl = 'https://api.datamuse.com/words';

  constructor(private httpClient: HttpClient) { }

  getSynonyms(word: string): Observable<string[]> {
    const params = {
      'rel_syn': word
    };
    return this.httpClient.get(this.baseUrl, {params})
      .pipe(map((synonyms: any[]) => {
        return synonyms
          .map(synonym => synonym.word)
          .filter(synonym => synonym.split(' ').length === 1);
      }));
  }
}
