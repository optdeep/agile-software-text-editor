import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { ITextStyling } from '../app.interfaces';

@Component({
  selector: 'app-control-panel',
  templateUrl: './control-panel.component.html',
  styleUrls: ['./control-panel.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ControlPanelComponent {
  @Input() styling: ITextStyling;
  @Output() stylingChange: EventEmitter<ITextStyling> = new EventEmitter<ITextStyling>();

  toggleStyling(stylingProperty: string, event: MouseEvent): void {
    this.stylingChange.emit({
      ...this.styling,
      [stylingProperty]: !this.styling[stylingProperty]
    });
  }
}
