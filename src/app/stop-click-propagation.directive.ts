import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[appStopClickPropagation]'
})
export class StopClickPropagationDirective {
  @HostListener('click', ['$event'])
  stopPropagation(event: MouseEvent): void {
    event.stopPropagation();
  }

}
