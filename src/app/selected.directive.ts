import { Directive, Input, ElementRef, OnChanges, HostListener } from '@angular/core';

@Directive({
  selector: '[appSelected]'
})
export class SelectedDirective implements OnChanges {
  @Input('appSelected') selected: boolean;

  constructor(private el: ElementRef) {}

  ngOnChanges(): void {
    this.updateSelection();
  }

  @HostListener('document:click')
  updateSelection(): void {
    if (window.getSelection && this.selected) {
      const selection = window.getSelection();
      const range = document.createRange();
      range.selectNodeContents(this.el.nativeElement);
      selection.removeAllRanges();
      selection.addRange(range);
    }
  }

}
