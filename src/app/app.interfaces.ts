export interface ITextStyling {
    bold: boolean;
    italic: boolean;
    underlined: boolean;
}

export interface IStyledString {
    content: string;
    styling: ITextStyling;
}
