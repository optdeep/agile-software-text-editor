import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { FileComponent } from './file/file.component';
import { ControlPanelComponent } from './control-panel/control-panel.component';
import { HeaderComponent } from './header/header.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'
import { TextService } from './text-service/text.service';
import { TextEditorComponent } from './text-editor/text-editor.component';
import { SelectedDirective } from './selected.directive';
import { SynonymsComponent } from './synonyms/synonyms.component';
import { StopClickPropagationDirective } from './stop-click-propagation.directive';

@NgModule({
  declarations: [
    AppComponent,
    FileComponent,
    ControlPanelComponent,
    HeaderComponent,
    TextEditorComponent,
    SelectedDirective,
    SynonymsComponent,
    StopClickPropagationDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [TextService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
