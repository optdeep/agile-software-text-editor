import { Injectable } from '@angular/core';
import { ITextStyling, IStyledString } from '../app.interfaces';

const DEFAULT_STYLING: ITextStyling  = {
  bold: false,
  italic: false,
  underlined: false
};

const INITIAL_TEXT = 'A year ago I was in the audience at a gathering of designers in San Francisco. ' +
'There were four designers on stage, and two of them worked for me. I was there to support them. ' +
'The topic of design responsibility came up, possibly brought up by one of my designers, I honestly don’t remember the details. ' +
'What I do remember is that at some point in the discussion I raised my hand and suggested, to this group of designers, ' +
'that modern design problems were very complex. And we ought to need a license to solve them.';

@Injectable()
export class TextService {
  defauldStyling: ITextStyling = DEFAULT_STYLING;
  getStylesWords(): IStyledString[] {
    return this.convertStringToStyledWords(INITIAL_TEXT);
  }

  private convertStringToStyledWords(text: string): IStyledString[] {
    return text
      .split(' ')
      .map(word => ({
        content: word,
        styling: this.defauldStyling
      }));
  }
}
