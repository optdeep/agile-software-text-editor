import { Component, Input, OnChanges, SimpleChanges, OnDestroy, Output, EventEmitter } from '@angular/core';
import { takeWhile } from 'rxjs/operators';
import { SynonymsService } from '../synonyms-service/synonyms.service';

@Component({
  selector: 'app-synonyms',
  templateUrl: './synonyms.component.html',
  styleUrls: ['./synonyms.component.css'],
  providers: [SynonymsService]
})
export class SynonymsComponent implements OnChanges, OnDestroy {
  @Input() word: string;
  @Output() synonymSelected: EventEmitter<string> = new EventEmitter<string>();

  synonyms: string[];
  private alive = true;

  constructor(private synonymsService: SynonymsService) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.word && changes.word.currentValue) {
      this.synonymsService.getSynonyms(this.word)
        .pipe(takeWhile(() => this.alive))
        .subscribe(synonyms => this.synonyms = synonyms);
    }
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  selectSynonym(synonym: string): void {
    this.synonymSelected.emit(synonym);
  }
}
