import { Component, Input, Output, EventEmitter } from '@angular/core';
import { IStyledString } from '../app.interfaces';

@Component({
  selector: 'app-file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.css']
})
export class FileComponent {
  @Input() words: IStyledString[];
  @Input() selectedWordIndex: number;
  @Output() wordSelected: EventEmitter<number> = new EventEmitter<number>();

  onDblClick(index: number) {
    this.wordSelected.emit(index);
  }

  isWordSelected(index: number): boolean {
    return index === this.selectedWordIndex;
  }
}
